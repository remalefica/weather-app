import './style.scss';

import { useContext } from 'react';
import { WeatherContext } from '../../../contexts/WeatherContext';

const DegreeSwitcher = () => {
  const { forecast } = useContext(WeatherContext);

  return (
    <div className="degree-switcher">
      <span id="temp">{forecast.tempC}</span>
      <sup>
        {/* <a href="#" id="metric" className="degree-active">℃</a> */}
        <span className='degree degree-active'>℃</span>
      </sup>
      <sup>
        {/* <a href="#" id="imperial">℉</a> */}
        <span className='degree degree-active'>℉</span>
      </sup>
    </div>
  );
}

export default DegreeSwitcher;